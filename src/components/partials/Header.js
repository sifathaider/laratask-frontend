import React,{Component} from 'react';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom'
import Products from '../Products';
import Login from "../auth/Login";
import Register from "../auth/Register";
import Admin from "../admin/Admin";
import './Header.css'

class Header extends Component{
    constructor(props) {
        super(props);
        this.state={
            auth:{},
            logInData:{},
            is_login:false
        }
    }
    componentDidMount() {
        let getData = localStorage.getItem('register')
        this.setState({ auth : getData ? JSON.parse(getData) : {} })
        let getLogInData = localStorage.getItem('login')
        this.setState({logInData : getLogInData ? JSON.parse(getLogInData) : {}})
        this.setState({is_login:this.state.logInData.login})
    }
    componentDidUpdate(prevProps, prevState, snapshot) {

    }

    logOutUser=()=>{
       let removeUser = localStorage.removeItem('login')
        this.setState({logInData:removeUser})
    }

    render() {
        return(
            <Router>
                <div>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <a className="navbar-brand" href="#">Task</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse d-flex justify-content-between align-item-center" id="navbarNavDropdown">
                            <div>
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/">Products</Link>
                                    </li>
                                </ul>
                            </div>
                            <div>
                                {
                                    !this.state.logInData.login ?
                                        <ul className="navbar-nav">
                                            <li className="nav-item">
                                                <Link className="nav-link" to='/login'>Login</Link>
                                            </li>
                                            <li className="nav-item">
                                                <Link className="nav-link" to="/register">Registration</Link>
                                            </li>
                                        </ul>
                                        :
                                        <ul className="navbar-nav">
                                            <li className="nav-item">
                                                <Link className="nav-link" onClick={()=>{this.logOutUser()}} to="/login">Log out</Link>
                                            </li>
                                        </ul>
                                }
                            </div>
                        </div>
                    </nav>
                </div>
                <div className="container-section">
                    <div className="row justify-content-center">
                        <div className="col-md-10">
                            <Route exact path="/" component={Products}/>
                            {
                                !this.state.logInData.login ?
                                    <Route exact path="/login" component={Login}/>
                                    :
                                    <Route exact path="/admin" component={Admin}/>
                            }
                            <Route exact path="/register" component={Register}/>
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}
export default Header;
