import React,{Component} from "react";
import {BrowserRouter as Router, Link, Route} from 'react-router-dom'
import './Admin.css'
import Category from './Category';
import Product from './Product'
import SubCategory from "./SubCategory";

class Admin extends Component{
    render() {
        return(
            <Router>
                <div className="row section-admin">
                    <div className="col-md-4 bg-success">
                        <div className="row justify-content-center">
                            <ul className="nav flex-column ">
                                <li className="nav-item">
                                    <Link className="nav-link" to="/admin/product">Products</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/admin/category">Category</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/admin/sub-category">Sub Category</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-md-8 bg-primary">
                        <div className="row justify-content-center">
                            <Route exact path="/admin/product" component={Product}/>
                            <Route exact path="/admin/category" component={Category}/>
                            <Route exact path="/admin/sub-category" component={SubCategory}/>
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}

export default Admin;