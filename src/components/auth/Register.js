import React,{Component} from 'react';
import Axios from 'axios'

class Register extends Component{
    constructor() {
        super();
        this.state={
            name:"",
            email:"",
            password:null,
            password_confirmation:null,
            loading:true,
            store:null,
            is_registerd:false
        }
    }
    registerUser=()=>{
        this.setState({loading:true})
        if(this.state.password === this.state.password_confirmation){
            Axios.post('http://127.0.0.1:8000/api/register',{
                name:this.state.name,
                email:this.state.email,
                password:this.state.password,
                password_confirmation:this.state.password_confirmation
            }).then(res=>{
                console.log(res);
                localStorage.setItem('register',JSON.stringify({
                    register_token:res.data.access_token,
                    user:res.data.user,
                    is_registerd:true
                }));
                this.setState({loading:false});
            }).catch(err=>console.log(err))
        }else{
            console.log('credential does not match')
            return;
        }
    }
    render() {
        return(
            <div>
                <h1 className="text-center mb-4">Registration</h1>
                <div className="form-group">
                    <label>Name</label>
                    <input className="form-control"
                           placeholder="Enter name" type="text"
                           onChange={(event) => {
                               this.setState({name: event.target.value})
                           }}/>
                </div>
                <div className="form-group">
                    <label>Email address</label>
                    <input className="form-control"
                           placeholder="Enter email" type="email"
                           onChange={(event) => {
                               this.setState({email: event.target.value})
                           }}/>
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input className="form-control" type="password" placeholder="Enter password" onChange={(event) => {
                        this.setState({password: event.target.value})
                    }}/>
                </div>
                <div className="form-group">
                    <label>Confirm Password</label>
                    <input className="form-control" type="password" placeholder="Confirm password" onChange={(event) => {
                        this.setState({password_confirmation: event.target.value})
                    }}/>
                </div>
                <button onClick={() => {
                    this.registerUser()
                }} type="submit" className="btn btn-primary">Register
                </button>
            </div>
        )
    }
}
export default Register;
