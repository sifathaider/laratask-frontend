import React,{Component} from 'react';
import Axios from 'axios';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom'
import Admin from "../admin/Admin";

class Login extends Component{
    constructor() {
        super();
        this.state={
            email:"",
            password:null,
            login:false,
            loading:true,
            store: {},
            redirect:null,
            is_login:false
        }
    }
    componentDidMount() {

    }

    loginUser=()=>{
        // fetch('http://127.0.0.1:8000/api/login',{
        //     method:"POST",
        //     body: JSON.stringify(this.state)
        // }).then((response)=>{
        //    response.json().then((result)=>{
        //        console.log("result",result);
        //        localStorage.setItem('login',JSON.stringify({
        //            login:true,
        //            token:result.token
        //        }))
        //    })
        // });
        Axios.post('http://127.0.0.1:8000/api/login',{
            email:this.state.email,
            password:this.state.password
        }).then(res=>{
            console.log(res);
            localStorage.setItem('login',JSON.stringify({
                login:true,
                token:res.data.access_token
            }));
            this.setState({login:true})
            this.setState({loading:false});
        }).catch(err=>console.log(err))
    }
    render() {
        return(
            <Router>
                <div>
                    <div>
                        <h1 className="text-center mb-4">Login</h1>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input className="form-control"
                                   placeholder="Enter email" type="email"
                                   onChange={(event) => {
                                       this.setState({email: event.target.value})
                                   }}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword">Password</label>
                            <input className="form-control" type="password"
                                   placeholder="Enter password" onChange={(event) => {
                                this.setState({password: event.target.value})
                            }}/>
                        </div>
                        <div className="form-group">
                            <Link onClick={() => {
                                this.loginUser()
                            }} className="btn btn-primary" to="/admin">Login</Link>
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}
export default Login;
